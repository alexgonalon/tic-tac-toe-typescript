import React, { useState } from "react";
import { Board } from "./Board";
import { CalculateWinner } from "./CalculateWinner";

export function Game () {
  const [history,setHistory] = useState([
    {
      squares: Array(9).fill(null)
    }
  ]);

  const [stepNumber,setStepNumber] = useState(0);
  const [isNext,setIsNext] = useState(true);

  function handleClick(i) {
    const jugadas = history.slice(0, stepNumber + 1);
    const current = jugadas[jugadas.length - 1];
    const squares = current.squares.slice();
    if (CalculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = isNext ? "X" : "O";

    setHistory( jugadas.concat([
      {
        squares: squares
      }
    ]))

    setStepNumber(jugadas.length)

    setIsNext (!isNext)

  }

  function jumpTo(step) {
    setStepNumber(step);
    setIsNext( (step % 2) === 0);
  }

    const jugadas = history;
    const current = jugadas[stepNumber];
    const winner = CalculateWinner(current.squares);

    const moves = jugadas.map((step, move) => {
      const desc = move ?
        'Go to move #' + move :
        'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (isNext ? "X" : "O");
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );

}
